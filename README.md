# transforming js
Microservice for transforming data in a pipe.

The service is based on the [pipe-connector](https://gitlab.com/european-data-portal/harvester/pipe-connector) library. Any configuration applicable for the pipe-connector can also be used for this service.

## Table of Contents
1. [Pipe Configuration](#pipe-configuration)
1. [Data Info Object](#data-info-object)
1. [Build](#build)
1. [Run](#run)
1. [Docker](#docker)
1. [License](#license)

## Pipe Configuration
The transformer can be configured through the config object as part of the segment body of the pipe.
It allows the configuration of either embedding the script directly in the config object or passing a references to a git repository:

```json
{
  "scriptType": "",
  "repository": {
    "uri": "",
    "branch": "",
    "username": "",
    "token": "",
    "script": ""
  },
  "script": ""
}
```
If `scriptType` is either `embedded`, which means the script is contained in the `script` field. The value `repository` indicates a script in a git repository which is described in more details in the repository field.

## Data Info Object
The transformer enriches the data info object with a hash value if not already contained.
The hash value is calculated based on the output of the transformation.

```json
{
  "hash": "value"
}
```

## Build

Requirements:
 * Git
 * Maven
 * Java

```bash
$ git clone https://gitlab.com/european-data-portal/harvester/tranforming-js.git
$ cd tranforming-js
$ mvn package
```
 
## Run

```bash
$ java -jar target/transforming-js-far.jar
```

## Docker

Build docker image:
```bash
$ docker build -t transforming-js .
```

Run docker image:
```^bash
$ docker run -it -p 8080:8080 tranforming-js
```

## License

[Apache License, Version 2.0](LICENSE.md)
