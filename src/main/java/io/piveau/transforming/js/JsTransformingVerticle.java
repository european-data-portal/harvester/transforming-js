package io.piveau.transforming.js;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.piveau.pipe.connector.PipeContext;
import io.piveau.transforming.repositories.GitRepository;
import io.piveau.utils.Hash;
import io.piveau.utils.JenaUtils;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.ext.web.client.WebClient;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.DatasetFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.*;
import java.io.*;
import java.nio.file.Path;
import java.time.Duration;

public class JsTransformingVerticle extends AbstractVerticle {

    private final Logger log = LoggerFactory.getLogger(getClass());

    public static final String ADDRESS = "io.piveau.pipe.transformation.js.queue";

    private Cache<String, ScriptEngine> cache;

    private WebClient client;

    @Override
    public void start(Future<Void> startFuture) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);
        client = WebClient.create(vertx);

        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                .withCache("transformer", CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, ScriptEngine.class,
                        ResourcePoolsBuilder.newResourcePoolsBuilder().heap(50, EntryUnit.ENTRIES))
                .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofHours(12))))
                .build(true);

        cache = cacheManager.getCache("transformer", String.class, ScriptEngine.class);

        startFuture.complete();
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();
        pipeContext.log().trace("Incoming pipe");
        ObjectNode config = (ObjectNode)pipeContext.getConfig();

        ObjectNode dataInfo = pipeContext.getDataInfo();
        if (dataInfo.hasNonNull("content") && dataInfo.get("content").asText().equals("identifierList")) {
            pipeContext.log().trace("Passing pipe");
            pipeContext.pass(client);
            return;
        }

        String runId = pipeContext.getPipe().getHeader().getRunId();
        ScriptEngine engine = cache.get(runId);
        if (engine == null) {
            String script = null;
            if ("repository".equalsIgnoreCase(config.path("scriptType").textValue())) {
                ObjectNode repository = (ObjectNode)config.path("repository");
                String uri = repository.path("uri").textValue();
                String branch = repository.path("branch").textValue();
                String username = repository.path("username").textValue();
                String token = repository.path("token").textValue();
                GitRepository gitRepo = GitRepository.open(uri, username, token, branch);
                Path file = gitRepo.resolve(repository.path("script").textValue());
                script = vertx.fileSystem().readFileBlocking(file.toString()).toString("UTF-8");
            } else {
                script = config.path("script").textValue();
            }

            try {
                engine = new ScriptEngineManager().getEngineByName("JavaScript");
                ScriptContext context = engine.getContext();
                context.setAttribute("name", "JavaScript", ScriptContext.ENGINE_SCOPE);

                engine.eval(script);
                engine.eval("function executeTransformation(obj) { return JSON.stringify(transforming(JSON.parse(obj))) }");

                JsonNode params = config.path("params");
                if (!params.isMissingNode()) {
                    engine.eval("var params = " + config.path("params").toString() + ";");
                }

                if (config.path("single").asBoolean()) {
                    cache.put(runId, engine);
                }
            } catch (ScriptException e) {
                log.error("initializing script template", e);
                pipeContext.log().error("Initialize script", e);
            }
        }

        ObjectNode info = pipeContext.getDataInfo();

        Invocable jsInvoke = (Invocable) engine;
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
            mapper.configure(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER, true);
            mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

            JsonNode input = mapper.readTree(pipeContext.getStringData());
            Object output = jsInvoke.invokeFunction("executeTransformation", input.toString());

            String out = output.toString();
            if (!info.hasNonNull("hash")) {
                info.put("hash", Hash.asHexString(out));
            }

            InputStream stream = new ByteArrayInputStream(out.getBytes("UTF-8"));

            Dataset dataset = DatasetFactory.create();
            try {
                RDFDataMgr.read(dataset, stream, Lang.JSONLD);
                Model model = dataset.getDefaultModel();
                model.setNsPrefixes(JenaUtils.DCATAP_PREFIXES);
                String outputFormat = config.path("outputFormat").asText("application/n-triples");
                String result = JenaUtils.write(model, outputFormat);
                pipeContext.setResult(result, outputFormat, info).forward(client);
            } catch (Exception e) {
                log.error("normalizing json-ld", e);
                pipeContext.log().error(info.toString(), e);
            }

            pipeContext.log().info("Data transformed: {}", info);

        } catch (IOException | NoSuchMethodException | ScriptException e) {
            log.error("transforming data", e);
            pipeContext.log().error(info.toString(), e);
        }

    }

}
